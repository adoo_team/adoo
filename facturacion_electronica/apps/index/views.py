from django.shortcuts import render, render_to_response

def index(request):
    return render(request, 'index/index.html')
#    return render(request, 'index.html')

'''
#   redirige a home si se logra loguear, pero es necesario estar logueado

@login_required(login_url='/')
def home(request):
    return render_to_response('index/home.html')

#   cierra su cesion
def logout(request):
    auth_logout(request)
    return redirect('/')

'''

