# Create your models here.


def variablesglobales(request):
    return {"MAXIMA_CANTIDAD_PROYECTO": 250000,
            "MINIMA_CANTIDAD_PROYECTO": 5000,
            "MINIMA_CANTIDAD_INVERSION": 500,
            "DIAS_PARA_EDICION": 7,
            "MINIMO_PORCENTAJE_INTERES": 5,
            "DIAS_PARA_EXTENSION_PROYECTO": 7,
            "PORCENTAJE_NECESARIO_EXTENSION": 90,
            }