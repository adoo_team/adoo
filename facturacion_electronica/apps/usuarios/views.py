# -*- encoding: utf-8 -*-
from django.core.mail import EmailMessage

from django.http import JsonResponse
from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth import login, logout
from django.views.generic import View, TemplateView
from .forms import LoginForm, SignUpForm, completar_perfil_form
from .models import Usuario, EmailOrUsernameModelBackend
from django.contrib.auth.decorators import login_required
from django.core import serializers


# Create your views here.                                                          'lista_facturas':lista_facturas })

@login_required

def perfil(request):
    #user_register = SignUpForm()
    datos_usuario=1
    if request.method == 'POST':
        completar_perfil = completar_perfil_form(request.POST)
        if completar_perfil.is_valid():
            guardar_informacion(request,completar_perfil)
            return redirect('/factura/nueva')

        return render(request, 'usuarios/perfil.html', {'datos_usuario': datos_usuario,
                                                        'completar_perfil': completar_perfil_form()})

    return render(request, 'usuarios/perfil.html', {'datos_usuario': datos_usuario,
                                                    'completar_perfil':completar_perfil_form()})


def guardar_informacion(request, new_profile):
    if new_profile.cleaned_data['nombres']:
        try:
            request.user.nombres = new_profile.cleaned_data['nombres']
        except:
            pass
    if new_profile.cleaned_data['apellidos']:
        try:
            request.user.apellidos = new_profile.cleaned_data['apellidos']
        except:
            pass
    if new_profile.cleaned_data['rfc']:
        try:
            request.user.rfc = new_profile.cleaned_data['rfc']
        except:
            pass
    if new_profile.cleaned_data['telefono']:
        try:
            request.user.telefono = new_profile.cleaned_data['telefono']
        except:
            pass
    if new_profile.cleaned_data['direccion']:
        try:
            request.user.direccion = new_profile.cleaned_data['direccion']
        except:
            pass
    request.user.save()
    print('guardamos')




def logIn(request):
    user_register = SignUpForm()
    login_form = LoginForm()
    if request.user.is_authenticated():
        return redirect('/factura/nueva')
    template='usuarios/login.html'
    redireccionar= '/factura/nueva'
    if request.method == 'POST':
        if 'register_form' in request.POST:
            user_register = SignUpForm(request.POST)
            if user_register.is_valid():
                user = Usuario.objects.create_user(
                    username=user_register.cleaned_data['username'].lower().strip(),
                    email=user_register.cleaned_data['email'],
                    password=user_register.cleaned_data['password'])

                try:
                    Email_bienvenido(user)
                except:
                    pass

                #sendEmail(user)
                autentificar = EmailOrUsernameModelBackend()
                user = autentificar.authenticate(username=user_register.cleaned_data['username'].lower().strip(),
                                                 password=user_register.cleaned_data['password'])
                if user is not None:
                    if user.is_active:
                        user.backend = 'django.contrib.auth.backends.ModelBackend'
                        login(request, user)
                        return redirect('/perfil')

        if 'login_form' in request.POST:
            login_form = LoginForm(request.POST)
            if login_form.is_valid():
                autentificar = EmailOrUsernameModelBackend()
                user = autentificar.authenticate(username=login_form.cleaned_data['username'].lower().strip(),
                                                 password=login_form.cleaned_data['password'])
                print(user)
                if user is not None:
                    print(user)
                    if user.is_active:
                        user.backend = 'django.contrib.auth.backends.ModelBackend'
                        login(request, user)
                        return redirect(redireccionar)
                    else:
                        user.is_active = True
                        login(request, user)
                        return render_to_response('index/index.html', context={
                            'desactivado': 'Gracias por volver a activar tu cuenta !! :D '})
                else:
                    return render(request, template, {'user_register': user_register,
                                                                   'login_form': login_form,
                                                                   'Error': "El username o password no son validos"})


    return render(request, template, {'user_register': user_register,
                                                   'login_form': login_form})


def logOut(request):
    logout(request)
    return redirect('/login')

def Email_bienvenido(user):
    # to=[request.user.email]
    msg = EmailMessage(subject='Bienvenido',
                       from_email='carlos.thrashaholic@gmail.com <carlos.thrashaholic@gmail.com>',
                       to=[user.email])
    msg.template_name = 'bienvenida.html'
    #si proporciono un username le enviaremos un correo 'mas personalizado' sino solo la bienvenida

    #'std_content00': '<h1>Hola %s Bienvenido a Somosrenka.com <br> gracias por registrarte' %user.username
    msg.template_content = {
        'std_username': 'Hola '+user.get_short_name,#enviamos el saludo a la persona
    }
    try:
        msg.send()
        ##print("se envio correo de bienvenida")
    except Exception as e :
        pass
        ##print("error : ", e )


