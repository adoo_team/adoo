# -*- encoding: utf-8 -*-
from django.forms import ModelForm

__author__ = 'metallica'

from django import forms
from django.conf import settings
from django.contrib.auth.forms import PasswordResetForm
from django.core.exceptions import ValidationError

from .models import Usuario


'''
                <div>
                    <a href="{% url 'social:begin' 'twitter' %}?next={{ request.path }}" class="btn btn-success btn-twitter ">
                        <i class="fa fa-twitter"> </i>
                    </a>
                    <a href="{% url 'social:begin' 'facebook' %}?next={{ request.path }}" class="btn btn-success btn-facebook ">
                        <i class="fa fa-facebook"> </i>
                    </a>
                    <a href="{% url 'social:begin' 'google-plus' %}?next={{ request.path }}" class="btn btn-success btn-google-plus ">
                        <i class="fa fa-google-plus"> </i>
                    </a>
                    <a href="{% url 'social:begin' 'linkedin' %}?next={{ request.path }}" class="btn btn-success btn-linkedin ">
                        <i class="fa fa-linkedin"> </i>
                    </a>
                    <br>
                </div>

'''
class LoginForm(forms.Form):
    username = forms.CharField(max_length=50,
                               widget=forms.TextInput(attrs={
                                   'placeholder': 'Nombre de Usuario/Email',
                                   'class': 'form-control',
                               }))
    password = forms.CharField(max_length=50,
                               widget=forms.TextInput(attrs={
                                   'type': 'password',
                                   'placeholder': 'Password',
                                   'class': 'form-control',
                               }))

    recordarme = forms.BooleanField(required=False,
                                    widget=forms.CheckboxInput())

    def clean(self):
        cleaned_data = self.cleaned_data
        print ("selfcleaned data : ", (self.cleaned_data))
        recordarme = self.cleaned_data.get('recordarme')
        print ("recordarme = ", recordarme)
        if not recordarme:
            settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = True
        else:
            settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = False
        return cleaned_data

class completar_perfil_form(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ('nombres', 'apellidos', 'email','rfc','telefono','direccion')
        widgets = {
            'nombres': forms.TextInput(attrs={
                'placeholder': 'Nombre de Usuario',
                'class': 'form-control',
            }),
            'apellidos': forms.TextInput(attrs={
                'type': 'text',
                'placeholder': 'Apellidos ..',
                'class': 'form-control',
            }),
            'email': forms.TextInput(attrs={
                'type': 'email',
                'placeholder': 'correo electronico',
                'class': 'form-control psw',
            }),
            'rfc': forms.TextInput(attrs={
                'type': 'text',
                'placeholder': 'tu RFC ',
                'class': 'form-control psw',
            }),
            'telefono': forms.TextInput(attrs={
                'type': 'text',
                'placeholder': 'Tu telefono de contacto',
                'class': 'form-control psw',
            }),
            'direccion': forms.TextInput(attrs={
                'type': 'text',
                'placeholder': 'Direccion completa ...',
                'class': 'form-control psw',
            }),
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        print(cleaned_data)
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super(completar_perfil_form, self).__init__(*args, **kwargs)
        #self.fields['username'].required = True
        self.fields['email'].type = 'email'
        self.fields['email'].required = False
        self.fields['nombres'].required = False
        self.fields['apellidos'].required = False
        self.fields['rfc'].required = False
        self.fields['telefono'].required = False
        self.fields['direccion'].required = False



class SignUpForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ('username', 'email', 'password')
        widgets = {
            'username': forms.TextInput(attrs={
                'placeholder': 'Nombre de Usuario',
                'class': 'form-control',
            }),
            'email': forms.TextInput(attrs={
                'type': 'email',
                'placeholder': 'Email',
                'class': 'form-control',
            }),
            'password': forms.TextInput(attrs={
                'type': 'password',
                'placeholder': 'password',
                'class': 'form-control psw',
            }),
        }

    def clean(self):
        cleaned_data = self.cleaned_data

        # if len(cleaned_data.get("username")) < 6:
        #     raise forms.ValidationError("El nombre de usuario debe contener mínimo 6 caracteres")
        # if len(cleaned_data.get("password")) < 6:
        #     raise forms.ValidationError("El password debe contener mínimo 6 caracteres")

        if Usuario.objects.filter(username=cleaned_data.get("username").lower()):
            print ("El nombre de usuario ya esta en uso")
            raise forms.ValidationError("El nombre de usuario ya esta en uso...")
        elif Usuario.objects.filter(username=cleaned_data.get("email").lower()):
            print ("El email ya esta en uso...")
            raise forms.ValidationError("El email ya esta en uso...")
        return cleaned_data


class EmailValidationOnForgotPassword(PasswordResetForm):
    def clean_email(self):
        email = self.cleaned_data['email']
        if not Usuario.objects.filter(email__iexact=email, is_active=True).exists():
            raise ValidationError("No tenemos tu correo registrado ! D= ")

        return email



