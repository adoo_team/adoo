from django.conf.urls import include, url

urlpatterns = [


    url(r'^login/$', 'apps.usuarios.views.logIn', name='login'),
    url(r'^logout/$', 'apps.usuarios.views.logOut', name='logout'),
    #url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^perfil/$', 'apps.usuarios.views.perfil', name='perfil'),


]
