#enconding: utf-8

from django.db import models
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils.datetime_safe import *

# Create your models here.
class UserManager(BaseUserManager, models.Manager):
    def _create_user(self, username, email, password, is_staff,
                     is_superuser, **extra_fields):
        email = self.normalize_email(email)
        # TWITTER Y LINKEDIN NO NOS DAN  el email del usuario, cpor estas dos lineas truena
        # if not email:
        # raise ValueError('El email debe ser obligatorio')
        user = self.model(username=username, email=email, is_active=True,
                          is_staff=is_staff, is_superuser=is_superuser, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email, password=None, **extra_fields):
        return self._create_user(username, email, password, False,
                                 False, **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        return self._create_user(username, email, password, True,
                                 True, **extra_fields)


class Usuario(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=20, unique=True)
    nombres = models.CharField(max_length=40)
    apellidos = models.CharField(max_length=40)
    fechaRegistro = models.DateField(default=datetime.now)
    email = models.EmailField()
    status = models.BooleanField(default=False)  # para verificar si ya hemos registrado al usuario o no con nosotros
    avatar = models.URLField(blank=True)  # necesario para captar la imagen  de perfil de su red social

    rfc = models.CharField(max_length=18)
    telefono = models.CharField(max_length=10)
    direccion= models.CharField(max_length=80)

    # Los siguientes 2 campos son requeridos para un modelo de usuarios
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = UserManager()
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    @property
    def get_short_name(self):
        if self.nombres != "":
            return self.nombres
        elif self.username != "":
            return self.username
        else:
            return self.email

    @property
    def get_full_name(self):
        if self.nombres != "":
            return self.nombres + " " + self.apellidos
        elif self.username != "":
            return self.username
        else:
            return self.email

    @property
    def get_image(self):
        return self.avatar

    def __str__(self):
        return str(self.nombres + "_" + self.username)

class persona_moral(models.Model):
    usuario= models.ForeignKey(Usuario)




class EmailOrUsernameModelBackend(ModelBackend):
    def authenticate(self, username=None, password=None):
        if '@' in username:
            kwargs = {'email': username}
        else:
            kwargs = {'username': username}
        try:
            user = Usuario.objects.get(**kwargs)
            if user.check_password(password):
                return user
        except Usuario.DoesNotExist:
            return None

    def get_user(self, username):
        try:
            return Usuario.objects.get(pk=username)
        except Usuario.DoesNotExist:
            return None

    def __str__(self):
        return str(self)



