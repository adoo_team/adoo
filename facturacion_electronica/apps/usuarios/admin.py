from django.contrib import admin
from .models import *

# Register your models here.


class Usuario_(admin.TabularInline):
    model = Usuario
    extra = 0


@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
    list_display = (
        'username',
        'nombres',
        'apellidos',
        'email',
        'is_staff')

    fieldsets = [
        ('Datos Personales', {'fields': ['username', 'nombres', 'apellidos']}),
        ('Otros Datos', {'fields': ['email']}),
    ]
    pass





