from django.contrib import admin
from .models import *

# Register your models here.



class Factura_(admin.TabularInline):
    model = Factura
    extra = 0


@admin.register(Factura)
class FacturaAdmin(admin.ModelAdmin):
    list_display = (
        'nombre',
        'usuario',
        'importe',
        'fecha',
        'descripcion')

    fieldsets = [
        ('datos relevante', {'fields': ['usuario', 'nombre', 'importe']}),
        ('otros ', {'fields': ['descripcion']}),
    ]
    pass


