from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from easymode.tree import xml
from apps.factura.forms import facturaForm
from apps.factura.models import Factura
from django.contrib.auth.decorators import login_required


def nueva(request):
    if not request.user.is_authenticated():
        return redirect('/login')
    nueva_factura = facturaForm()
    if request.method == 'POST':
        if 'factura_form' in request.POST:
            nueva_factura = facturaForm(request.POST)
            if nueva_factura.is_valid():
                print("form valida")
                factura= Factura.objects.create(usuario=request.user,
                                                nombre=nueva_factura.cleaned_data['nombre'].lower().strip(),
                                                importe=nueva_factura.cleaned_data['importe'],
                                                descripcion=nueva_factura.cleaned_data['descripcion'].strip(),
                                                )
            exito = 'Agregaste con exito una factura :D'
            return render(request, 'factura/nueva.html', {'exito': exito,'nueva_factura':facturaForm()})

    return render(request, 'factura/nueva.html', {'nueva_factura': nueva_factura,'exito':'',})



def lista_facturas(request):
    if not request.user.is_authenticated():
        return redirect('/login')
    lista_facturas = Factura.objects.filter(usuario=request.user)
    exito=''
    return render(request, 'factura/lista_facturas.html', {'exito': exito,'lista_facturas':lista_facturas })


def detalles_factura(request,factura_id):
    if not request.user.is_authenticated():
        return redirect('/login')
    factura = get_object_or_404(Factura, pk=factura_id)
    extra=1
    return render(request, 'factura/detalle_factura.html', {'factura': factura,
                                                           'extra':extra,
                                                           })

from yattag import Doc, indent
from django.utils.encoding import smart_str

def detalle_facturas_xml(request):
    from django.http import HttpResponse
    from django.core.servers.basehttp import FileWrapper
    # generate the file
    facturas = Factura.objects.filter(usuario=request.user)

    factura_xml = crear_xml(request.user, facturas)
    nombre_archivo= 'media/xml/'+str(request.user)+'_facturas.xml'
    archivo = open(nombre_archivo,'w')
    archivo.write(factura_xml)
    print(factura_xml)
    #'''
    response = HttpResponse(factura_xml, content_type='application/xml')
    response['Content-Disposition'] = 'attachment; filename='+nombre_archivo
    #archivo.close()
    return response

    '''
    print(factura_xml)
    response = HttpResponse(content_type='application/force-download')
    response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(nombre_archivo)
    response['X-Sendfile'] = smart_str(archivo)
    # It's usually a good idea to set the 'Content-Length' header too.
    # You can also set any other required headers: Cache-Control, etc.
    return response
    '''


def detalle_factura_xml(request, accion ,id_factura):
    from django.http import HttpResponse
    from django.core.servers.basehttp import FileWrapper
    # generate the file
    if accion=='xml':
        try:
            fac = Factura.objects.get (id=id_factura)
            if fac:
                factura = crear_xml_factura(fac,id_factura)
                nombre_archivo= 'media/xml/'+str(request.user.username)+'-factura_'+str(id_factura)+'.xml'
                archivo = open(nombre_archivo,'w')
                archivo.write(factura)
                print(factura)
                #'''
                response = HttpResponse(factura, content_type='application/xml')
                response['Content-Disposition'] = 'attachment; filename='+nombre_archivo.split('/')[-1]
                #archivo.close()
                return response

            lista_facturas = Factura.objects.filter(usuario=request.user)
            exito=''
            return render(request, 'factura/lista_facturas.html', {'exito': exito,'lista_facturas':lista_facturas })

        except Exception as e:
            print (e)
            lista_facturas = Factura.objects.filter(usuario=request.user)
            exito=''
            return render(request, 'factura/lista_facturas.html', {'exito': exito,'lista_facturas':lista_facturas })

    elif accion=='borrar':
        try:
            factura = Factura.objects.filter(id=id_factura)
            if factura:
                factura.delete()
                print('se elimino la factura')

                lista_facturas = Factura.objects.filter(usuario=request.user)
                exito=''
                return render(request, 'factura/lista_facturas.html', {'exito': exito,'lista_facturas':lista_facturas })
            lista_facturas = Factura.objects.filter(usuario=request.user)
            exito=''
            return render(request, 'factura/lista_facturas.html', {'exito': exito,'lista_facturas':lista_facturas })
        except:
            lista_facturas = Factura.objects.filter(usuario=request.user)
            exito=''
            return render(request, 'factura/lista_facturas.html', {'exito': exito,'lista_facturas':lista_facturas })

def crear_xml(usuario, facturas):
    doc, tag, text = Doc().tagtext()

    with tag('Usuario',name=usuario.username):
        with tag('Informacion_Personal'):
            with tag('nombre'):
                text(usuario.nombres)
            with tag('Apellidos'):
                text(usuario.apellidos)
            with tag('email'):
                text(usuario.email)
            with tag('RFC'):
                text(usuario.rfc)
            with tag('Telefono'):
                text(usuario.telefono)
            with tag('Direccion'):
                text(usuario.direccion)
        if facturas:
            with tag('Facturas'):
                for factura in facturas:
                    with tag('Factura', id=str(factura.id)):
                        with tag('Nombre'):
                            text(factura.nombre)
                        with tag('fecha'):
                            text(str(factura.fecha))
                        with tag('descripcion'):
                            text(factura.descripcion)
                        with tag('importe'):
                            text(str(factura.importe))

    result = indent(
        doc.getvalue(),
        indentation = ' '*4,
        newline = '\r\n'
    )

    return result

##########
def crear_xml_factura(factura,id_factura):
    doc, tag, text = Doc().tagtext()

    with tag('Factura', id=str(id_factura)):
        with tag('Nombre'):
            text(factura.nombre)
        with tag('fecha'):
            text(str(factura.fecha))
        with tag('descripcion'):
            text(factura.descripcion)
        with tag('importe'):
            text(str(factura.importe))

    result = indent(
        doc.getvalue(),
        indentation = ' '*4,
        newline = '\r\n'
    )

    return result

def enviar_correo_factura(request,factura_id):
    if not request.user.is_authenticated():
        return redirect('/login')
    factura = get_object_or_404(Factura, pk=factura_id)
    extra=1
    return render(request, 'factura/envio_correo_exito.html', {'factura': factura,
                                                           'extra':extra,
                                                           })


#'''
from easy_pdf.views import PDFTemplateView
class factura_pdf(PDFTemplateView):
    #template_name = "factura/lista_facturas.html"
    #template_name = "usuarios/perfil.html"
    template_name = "factura/../../templates/factura/factura_pdf.html"
#'''



