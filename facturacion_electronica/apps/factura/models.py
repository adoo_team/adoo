#enconding: utf-8

from django.db import models
from apps.usuarios.models import Usuario
from django.utils.datetime_safe import *
from easymode.tree.decorators import toxml


# Create your models here.

@toxml
class Factura (models.Model):
    nombre = models.CharField(max_length=40)
    usuario = models.ForeignKey(Usuario)
    fecha = models.DateField(default=datetime.now)
    status = models.BooleanField(default=False)  #
    importe = models.FloatField(default=0.0)
    descripcion = models.CharField(max_length=300)







    def __str__(self):
        return str(self.nombre + ": " + self.usuario.nombres)

