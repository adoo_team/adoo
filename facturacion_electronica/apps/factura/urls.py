from apps.factura.views import factura_pdf

__author__ = 'metallica'

from django.conf.urls import include, url

urlpatterns = [
    #Linea comentada para evitar errores de import
    url(r"^factura/pdf/", factura_pdf.as_view()),

    url(r'^factura/historial/', 'apps.factura.views.lista_facturas', name='lista_facturas'),
    url(r'^factura/nueva/', 'apps.factura.views.nueva', name='nueva'),
    url(r'^factura/(?P<factura_id>\d+)/', 'apps.factura.views.detalles_factura', name='detalles_factura'),
    url(r'^correo/(?P<factura_id>\d+)/', 'apps.factura.views.enviar_correo_factura', name='enviar_correo_factura'),

    url(r'^factura/historial_xml/', 'apps.factura.views.detalle_facturas_xml', name='facturas_xml'),
    url(r'^factura/(?P<accion>\w+)/(?P<id_factura>\d+)', 'apps.factura.views.detalle_factura_xml', name='acciones_factura'),



    #url(r'^factura/historial/', 'apps.factura.views.lista_facturas', name='lista_facturas'),

    #url('', include('social.apps.django_app.urls', namespace='social')),

]
