#enconding: utf-8
__author__ = 'metallica'
from apps.factura.models import Factura
from django import forms




class facturaForm(forms.ModelForm):
    class Meta:
        model = Factura
        fields = ('nombre', 'importe','descripcion')
        widgets = {
            'nombre': forms.TextInput(attrs={
                'placeholder': 'Nombre de la factura',
                'class': 'form-control',
            }),
            'importe': forms.TextInput(attrs={
                'placeholder': 'importe',
                'class': 'form-control',
            }),
            'descripcion': forms.Textarea(attrs={
                'type': 'text',
                'placeholder': 'descripcion de la operacion',
                'class': 'form-control psw',
            }),
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        return cleaned_data






