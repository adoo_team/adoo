
$(document).on('scroll', function(){
    if ($(this).scrollTop() > $(".header .header-descr").offset().top - 250) {
        $('.navbar').removeClass('navbar-dark').addClass('navbar-light');
        $('.navbar-light .navbar-brand img').attr("src", "/static/base/img/logo-renka-dark.png");
    }
    else{
        $('.navbar').removeClass('navbar-light').addClass('navbar-dark');
        $('.navbar-dark .navbar-brand img').attr("src", "/static/base/img/logo-renka-white.png");
    }

});

function changeColor(element, curNumber){
    curNumber++;

    if(curNumber > 4){
        curNumber = 1;
    }
    if (curNumber == 1){
            element.removeClass('color4');
    }
    else{
        element.removeClass('color' + (curNumber-1).toString());
    }

    element.addClass('color' + curNumber, 4000);

    setTimeout(function(){changeColor(element, curNumber)}, 8000);
}

$(window).load(function () {
    $('.navbar').removeClass('navbar-light').addClass('navbar-dark');
    $('.navbar-brand img').attr("src", "/static/base/img/logo-renka-white.png");
});

$(document).ready(function () {
    changeColor($('.color1'), 0);

});
