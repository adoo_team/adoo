from django.conf.urls import include, url, patterns
from django.contrib import admin
from apps.usuarios.forms import EmailValidationOnForgotPassword


urlpatterns = [
    # Examples:
    # url(r'^$', 'facturacion_electronica.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('apps.index.urls', namespace='index_app')),
    url(r'^', include('apps.usuarios.urls', namespace='usuarios_app')),
    url(r'^', include('apps.factura.urls', namespace='factura_app')),



    # Urls para re establecer el password
    url(r'^reset/password_reset/$', 'django.contrib.auth.views.password_reset',
        {'password_reset_form': EmailValidationOnForgotPassword}, name='reset_password_reset1'),
    url(r'^reset/password_reset/done/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm',
        name='password_reset_confirm'),
    url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete', name='password_reset_complete'),

    # login with python social auth
    #   url('', include('social.apps.django_app.urls', namespace='social')),


]
